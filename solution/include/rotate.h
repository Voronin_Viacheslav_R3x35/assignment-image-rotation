#pragma once
#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image rotate_image(const struct image original_image);
