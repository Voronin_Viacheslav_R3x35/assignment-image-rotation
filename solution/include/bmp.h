#pragma once
#include "util.h"
#include <stdint.h>
#include <string.h>

struct __attribute__((packed)) bmp_header {
        uint16_t bfType;
        uint32_t  bfileSize;
        uint32_t bfReserved;
        uint32_t bOffBits;
        uint32_t biSize;
        uint32_t biWidth;
        uint32_t  biHeight;
        uint16_t  biPlanes;
        uint16_t biBitCount;
        uint32_t biCompression;
        uint32_t biSizeImage;
        uint32_t biXPelsPerMeter;
        uint32_t biYPelsPerMeter;
        uint32_t biClrUsed;
        uint32_t  biClrImportant;
};

struct bmp{
    struct bmp_header header;
    struct pixel* pixels;
};

struct bmp read_bmp(FILE *fp);

struct image conv_bmp_to_image(const struct bmp bmp_to_conv);

struct bmp conv_image_to_bmp(const struct image image_to_conv);

void write_bmp(FILE *fp, struct bmp bmp_to_write);
