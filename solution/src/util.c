#include "util.h"
#include <stdio.h>
#include <stdlib.h>

void print_errors(const char* text) {
    fprintf(stderr, "%s\n", text);
    exit(1);
}
