#include "image.h"
#include <stdint.h>
#include <stdlib.h>

struct image image_init(uint32_t width, uint32_t height){
    struct image workname_image;
    workname_image.width = width;
    workname_image.height = height;
    workname_image.data = malloc(sizeof(struct pixel)*width*height);
    return workname_image;
}
