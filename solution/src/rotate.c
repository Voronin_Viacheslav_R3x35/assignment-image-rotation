#include "rotate.h"
#include "image.h"
#include <stdint.h>
#include <stdlib.h>



struct image rotate_image(const struct image original_image){
    struct image working_image = image_init(original_image.height, original_image.width);
    for (size_t i = 0; i < working_image.height; i++) {
        for (size_t j = 0; j < working_image.width; j++) {
            working_image.data[i*working_image.width+j] = original_image.data[(working_image.width-j-1)*original_image.width+i];
    }
    }
    return working_image;
}
