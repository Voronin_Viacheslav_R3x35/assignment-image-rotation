#include "bmp.h"
#include "image.h"
#include "rotate.h"
#include "util.h"
#include <stdint.h>
#include <stdio.h>


int main( int argc, char** argv ) {
    if (argc != 3) print_errors("incorrect arguments!");
    
    FILE *fpr = fopen(argv[1], "rb");
    if (!fpr) print_errors("can't read file!");

    struct bmp r_bmp = read_bmp(fpr);
    if (fclose(fpr)) print_errors("Can't close file!");

    struct image conv_image = conv_bmp_to_image(r_bmp);

    struct image res_image = rotate_image(conv_image);

    struct bmp res_bmp = conv_image_to_bmp(res_image);

    FILE *fpw = fopen(argv[2], "wb");
    if (!fpw) print_errors("can't write file!");

    write_bmp(fpw, res_bmp);
    if (fclose(fpw)) print_errors("Can't close file!");

    free(r_bmp.pixels);
    free(res_bmp.pixels);
}
