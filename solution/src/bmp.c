#include "bmp.h"
#include "util.h"
#include <stdint.h>
//#include <string.h>
#include "image.h"

static inline uint8_t padding(uint32_t width){
    return width%4;
}

static struct bmp_header header_init(uint32_t width, uint32_t height){
    uint8_t pad = padding(width);
    uint32_t size_image = (width*sizeof(struct pixel) + pad) * height;
    uint32_t file_size = sizeof(struct bmp_header) + size_image;
    struct bmp_header head_init = {
        .bfType = 19778,
        .bfileSize = file_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = 40,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = size_image,
        .biXPelsPerMeter = 2834,
        .biYPelsPerMeter = 2834,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
    return head_init;
}

struct bmp read_bmp(FILE *fp){
    struct bmp bmp_new;
    if(!fread(&bmp_new.header, sizeof(struct bmp_header), 1, fp)) print_errors("Can't read bmp header");

//  я делал проверку на валидность заголовка, но из-за ошибки в тестах это не имеет смысла
//  struct bmp_header header_to_cmp = header_init(bmp_new.header.biWidth, bmp_new.header.biHeight);
//  int res = memcmp(&bmp_new.header, &header_to_cmp, sizeof(struct bmp_header));
//  printf("%d", res);
//  if (res!=0) print_errors("bad header!");
/*  printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", bmp_new.header.bfType, bmp_new.header.bfileSize, bmp_new.header.bfReserved, bmp_new.header.bOffBits, bmp_new.header.biSize, bmp_new.header.biWidth, bmp_new.header.biHeight, bmp_new.header.biPlanes, bmp_new.header.biBitCount, bmp_new.header.biCompression, bmp_new.header.biSizeImage, bmp_new.header.biXPelsPerMeter, bmp_new.header.biYPelsPerMeter, bmp_new.header.biClrUsed, bmp_new.header.biClrImportant);
    printf("\n");
    printf("%d %d %d %d %d %d %d %d %d %d %d %d %d %d %d", header_to_cmp.bfType, header_to_cmp.bfileSize, header_to_cmp.bfReserved, header_to_cmp.bOffBits, header_to_cmp.biSize, header_to_cmp.biWidth, header_to_cmp.biHeight, header_to_cmp.biPlanes, header_to_cmp.biBitCount, header_to_cmp.biCompression, header_to_cmp.biSizeImage, header_to_cmp.biXPelsPerMeter, header_to_cmp.biYPelsPerMeter, header_to_cmp.biClrUsed, header_to_cmp.biClrImportant);
    printf("\n");*/

//  Ю.О. Жирков сказал проверять валидность таким образом
    if (bmp_new.header.biBitCount != 24) print_errors("bad header!");

    uint8_t pad = padding(bmp_new.header.biWidth);
    bmp_new.pixels = malloc(sizeof(struct pixel)*bmp_new.header.biWidth*bmp_new.header.biHeight);
    uint32_t width_len;

    for (size_t i = 0; i < bmp_new.header.biHeight; i++){
        width_len = fread(&(bmp_new.pixels[i*bmp_new.header.biWidth]), sizeof(struct pixel), bmp_new.header.biWidth, fp);
        int f_res = fseek(fp, pad, SEEK_CUR);
        if (f_res != 0 || bmp_new.header.biWidth != width_len) print_errors("can't read bmp pixels");
    }

    return bmp_new;
}

struct image conv_bmp_to_image(const struct bmp bmp_to_conv){
    struct image image_conv = {.width = bmp_to_conv.header.biWidth, 
                                  .height = bmp_to_conv.header.biHeight, 
                                  .data = bmp_to_conv.pixels};

    return image_conv;
}

struct bmp conv_image_to_bmp(const struct image image_to_conv){
    struct bmp bmp_conv = {.header = header_init(image_to_conv.width, image_to_conv.height),
                              .pixels = image_to_conv.data};
    return bmp_conv;
}

void write_bmp(FILE *fp, struct bmp bmp_to_write){
    if(!fwrite(&bmp_to_write.header, sizeof(struct bmp_header), 1, fp)) print_errors("Can't write header!");
    uint8_t pad = padding(bmp_to_write.header.biWidth);
    uint32_t width_len;
    const uint64_t zero = 0;
    for (size_t i = 0; i < bmp_to_write.header.biHeight; i++){
        width_len = fwrite(&(bmp_to_write.pixels[i*bmp_to_write.header.biWidth]), sizeof(struct pixel), bmp_to_write.header.biWidth, fp);
        unsigned long f_res = fwrite(&zero, 1, pad, fp);
        if (f_res != pad || bmp_to_write.header.biWidth != width_len) print_errors("can't write bmp pixels");
    }
}
